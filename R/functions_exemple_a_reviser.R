# Les dependances
library(ggplot2)
library(tidyr)
library(dplyr)
# library(thinkridentity)
library(flextable)

#' Affichage beau tableau de la section à réviser
#'
#' @param x le tableau
#' @param caption le legende du tableau
affiche_tableau <- function(x, caption) {
  ft <- flextable(x)
  ft <- set_caption(ft,
                    caption = caption,
                    html_escape = FALSE)
  ft <- set_table_properties(ft, width = 1, layout = "autofit")
  ft
}

#' Affichage du graphique
#' @param x le tableau des prévisions
affiche_graphique_prevision <- function(x) {
  x %>%
    pivot_longer(-branche, names_to = "name", values_to = "value") %>%
    separate(name, into = c("annee", "quarter"), sep = "_", remove = FALSE) %>%
    mutate(trimestre = case_when(
      quarter == 'T1' ~ paste0(annee, "-01-01"),
      quarter == 'T2' ~ paste0(annee, "-04-01"),
      quarter == 'T3' ~ paste0(annee, "-07-01"),
      quarter == 'T4' ~ paste0(annee, "-10-01")
    ) %>% as.Date()
    ) %>%
    ggplot() +
    geom_col(aes(x = trimestre, y = value, fill = branche),
            position = position_dodge()) +
    # scale_fill_thinkr_d(guide = guide_legend(nrow = 2, byrow = TRUE)) +
    scale_fill_manual(values = c("#15b7d6", "#f15522", "#7176b8",
                                 "#f66a8d", "#b7d615", "#ada9ae", "#38424f"),
                      guide = guide_legend(nrow = 2, byrow = TRUE)
    ) +
    theme_minimal() +
    theme(legend.position = "bottom")

}
