# Exercice pratique 

Ce projet a vocation à être utilisé pour un exercice.  
Choisissez trois personnes représentant:

- *[Prems]* L'équipe de rédaction et/ou développement R
- *[Rel]* Une personne chargée de la première relecture
- *[RedacChf]* Une personne chargée du second niveau de relecture

Suivez les instructions de l'exercice directement dans le chapitre "a - Exemple de section de texte à réviser" du book : <https://thinkr-open.gitlab.io/demo.test.thinkr/main>

## Instructions générales de rédaction

Les CR sont rédigés de manière collaborative sur GitLab directement ou bien depuis une session RStudio locale, chacun dans sa branche.  
Les révisions du contenu s'opèrent par l'intermédiaire de "Merge Requests" (Demande de fusion) vers la branche principale.

Le rapport est compilé et généré automatiquement dès fusion vers la branche principale. La dernière version est accessible ici: <> 
Cette version peut-être imprimée en PDF en l'état.

On explore un processus complet en suivant le guide réalisé pour les DREAL: <https://rdes_dreal.gitlab.io/publication_guide/collaborer-git.html>.
Il sera ensuite adapté à votre utilisation.
Nous pourrons identifier les points qui nécessitent une formation dédiée.

Le principe : chacun développe dans une branche qui est reversée dans *main*

-   Des branches temporaires pour les premiers rédacteurs
-   Une branche *validation* régulièrement mise à jour pour le premier relecteur
-   Une branche *production* régulièrement mise à jour pour le rédacteur en chef

Ainsi, on peut continuer d'alimenter le *main* même s'il y a un processus de révision en cours.

## Génération automatique des documents

Les documents sont générés automatiquement grâce à un dispositif qui s'appelle
l'intégration continue. La dernière version de ces documents est disponible en
cliquant ici :
*<https://thinkr-open.gitlab.io/demo.test.thinkr/main>*

## Un rapport pour chaque branche importante

Le rapport est compilé et généré automatiquement dès fusion vers la branche principale (`main`). La dernière version est accessible ici: <https://thinkr-open.gitlab.io/demo.test.thinkr/main>
Cette version peut-être imprimée en PDF en l'état.

Chaque branche spécifique comme `production` ou `validation` montre une version du rapport dans cette branche :

- Pour `production`: <https://thinkr-open.gitlab.io/demo.test.thinkr/production>
- Pour `validation`: <https://thinkr-open.gitlab.io/demo.test.thinkr/validation>
Toutes les versions de branches spécifiques sont visibles dans l'index: <https://thinkr-open.gitlab.io/demo.test.thinkr/index.html>


_Pour changer l'apparence des sortie HTML (couleurs, espaces, ...), il est nécessaire de modifier les ressources css de ce projet ou bien de créer un package dédié._

## Un rapport au format odt à jour

Le rapport au format "odt" peut être téléchargé dans sa dernière version ici: <https:://https://thinkr-open.gitlab.io/demo.test.thinkr/main/rapport.odt>

_Note : Pour ce format, vous avez la possibilité de changer le template par défaut inclu dans le 'pandoc/reference.odt'_
