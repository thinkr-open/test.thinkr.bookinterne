# b - Contenu des tickets GitLab pour l'exemple de section de texte à réviser

## Connecter au GitLab
```{r, eval=FALSE}
library(gitlabr)
```

- Create environment variable `GITLAB_COM_TOKEN` with api token on <https://gitlab.com/>
  + Store it in `".Renviron"`
  + `usethis::edit_r_environ()`

- Exemple there is for a project named {demo.test.thinkr}, project `32952698` on our GitLab.

> Tester avec GITLAB: GITLAB_COM_TOKEN

```{r, eval=FALSE}
gitlab_url <- "https://gitlab.com/"
# GitLab con
my_gitlab <- gl_connection(
  gitlab_url = gitlab_url,
  private_token = Sys.getenv("GITLAB_COM_TOKEN")
)

# Set the connection for the session
set_gitlab_connection(my_gitlab)

```

Find your project id

```{r, eval=FALSE}
# Pour demo.test.thinkr, project id 435
project_id <- 32952698
# Get number of existing issues
proj_issues <- gl_list_issues(project = project_id)
n_proj_issues <- nrow(proj_issues)
```

## Preparer le Boards

- Créer des labels de tickets
```{r, eval=FALSE}
tibble::tribble(
    ~name, ~color, ~description,
    "A valider", "#D10069", "Validation client pour mise en ligne",
    "Bloqu\u00E9", "#7F8C8D", "N\u00E9cessite des infos compl\u00E9mentaires pour \u00EAtre trait\u00E9",
    "En cours", "#428BCA", "En cours de traitement",
    "Pr\u00EAt", "#69D100", "Pr\u00EAt \u00E0 \u00EAtre trait\u00E9",
    "En attente", "#A8D695", "A ce qu'il faut pour \u00EAtre trait\u00E9e, mais il y a trop de choses dans 'Pr\u00EAt'",
    "R\u00E9vision", "#F0AD4E", "A r\u00E9viser avant int\u00E9gration dans master"
  )

# Manuellement dans GitLab
# Ou avec l'outil interne à ThinkR
thinkridentity::add_labels(project_id)
```

```{r, eval=TRUE}
if (!exists("n_proj_issues")) {
  n_proj_issues <- 2
}
```

## Instructions pour *[Prems-1]*

```{r, eval=TRUE}
issue_dev <- paste(
'Cocher les cases au fur et à mesure

- [ ] Lire le chapitre "a - Exemple de section de texte à réviser"
  + [ ] https://gitlab.com/thinkr-open/demo.test.thinkr
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "En cours"
- [ ] Créer une branche à partir de ce ticket
- [ ] Récupérer le projet sur RStudio
- [ ] Compiler le book en local pour voir s\'il fonctionne en l\'état
    + Onglet Build > Build Book > "pagedown::html_paged"
- [ ] Changer le code de la fonction `affiche_graphique_prevision()` pour que le nom de la colonne des `quarters` s\'appelle `trimestre`.
    + Il se trouve dans le dossier "R/"
- [ ] Changer la légende du graphique en conséquence dans le texte de "zza-exemple-de-section-a-reviser.Rmd"
- [ ] Compiler le book en local
- [ ] Vérifier que la compilation est ok et que les modifications ont été apportées
- [ ] Créer un commit qui indique ce qu\'il fait et de fermer cette issue',
paste0('```'),
'Modification de affiche_graphique_prevision

- Traduction en français',
paste0('- closes #', n_proj_issues + 1),
paste0('```'),
'- [ ] Envoyer les modifications sur GitLab
- [ ] S\'assurer que l\'intégration continue est au vert
- [ ] Assigner la révision de la MR à *[Prems-2]*
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "Révision"
- [ ] Mettre un message à *[Prems-2]* dans Slack pour lui indiquer qu\'il a une MR à réviser
- [ ] Fermer ce ticket quand la MR est acceptée
', sep = "\n")

```

```{r eval=FALSE}
issue_dev_id <- gl_new_issue(title = "Instructions pour *[Prems-1]*",
                             project = project_id,
                             description = paste(issue_dev, collapse = "\n"),
                             labels = c("Bloqu\u00E9"))
```
```{r, eval=TRUE}
glue::glue(issue_dev)
```

## Instructions pour *[Prems-2]*

```{r}
issue_dev <- paste(
'Cocher les cases au fur et à mesure

- [ ] Lire le chapitre "a - Exemple de section de texte à réviser"
  + [ ] https://gitlab.com/thinkr-open/demo.test.thinkr
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "En cours"
- [ ] Créer une branche à partir de ce ticket
- [ ] Récupérer le projet sur RStudio
- [ ] Compiler le book en local pour voir s\'il fonctionne en l\'état
    + Onglet Build > Build Book > "pagedown::html_paged"
- [ ] Ajouter une ligne pertinente au tableau `les_data_prevision` dans le chapitre   
    + Ce tableau est créé directement dans le texte de "zza-exemple-de-section-a-reviser.Rmd"
- [ ] Compiler le book en local
- [ ] Vérifier que la compilation et que les graphiques et tableaux sont bien modifiés.
- [ ] Créer un commit qui indique ce qu\'il fait et de fermer cette issue',
paste0('```'),
'Modification de les_data_prevision

- Ajout d\'une nouvelle section de prévision',
paste0('- closes #', n_proj_issues + 2),
paste0('```'),
'- [ ] Envoyer les modifications sur GitLab
- [ ] S\'assurer que l\'intégration continue est au vert
- [ ] Assigner la révision de la MR à *[Prems-3]*
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "Révision"
- [ ] Mettre un message à *[Prems-3]* dans Slack pour lui indiquer qu\'il a une MR à réviser
- [ ] Fermer ce ticket quand la MR est acceptée
', sep = "\n")
```

```{r eval=FALSE}
issue_dev_id <- gl_new_issue(title = "Instructions pour *[Prems-2]*",
                             project = project_id,
                             description = paste(issue_dev, collapse = "\n"),
                             labels = c("Bloqu\u00E9"))
```

```{r}
glue::glue(issue_dev)
```

## Instructions pour *[Prems-3]*

```{r, eval=TRUE}
issue_dev <- paste(
'Cocher les cases au fur et à mesure

- [ ] Lire le chapitre "a - Exemple de section de texte à réviser"
  + [ ] https://gitlab.com/thinkr-open/demo.test.thinkr
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "En cours"
- [ ] Créer une branche à partir de ce ticket
- [ ] Récupérer le projet sur RStudio
- [ ] Compiler le book en local pour voir s\'il fonctionne en l\'état
    + Onglet Build > Build Book > "pagedown::html_paged"
- [ ] Extraire une nouvelle variable à afficher dans le texte de manière similaire à `val_construction_t1` directement dans le texte de "zza-exemple-de-section-a-reviser.Rmd"
    + [ ] Cette variable récupère la valeur de T1 pour la branche "Industrie": `val_industrie_t1`
- [ ] Ajouter cette variable dans le texte dans la même phrase et de la même manière que pour la variable `val_construction_t1`
- [ ] Compiler le book en local
- [ ] Vérifier que la compilation et que les graphiques et tableaux sont bien modifiés.
- [ ] Créer un commit qui indique ce qu\'il fait et de fermer cette issue',
paste0('```'),
'Ajout d\'une variable \'val_construction_t1\'

- La variable est utilisée dans le texte directement',
paste0('- closes #', n_proj_issues + 3),
paste0('```'),
'- [ ] Envoyer les modifications sur GitLab
- [ ] S\'assurer que l\'intégration continue est au vert
- [ ] Assigner la révision de la MR à *[Prems-1]*
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "Révision"
- [ ] Mettre un message à *[Prems-1]* dans Slack pour lui indiquer qu\'il a une MR à réviser
- [ ] Fermer ce ticket quand la MR est acceptée
', sep = "\n")
```


```{r, eval=FALSE}
issue_dev_id <- gl_new_issue(title = "Instructions pour *[Prems-3]*",
                             project = project_id,
                             description = paste(issue_dev, collapse = "\n"),
                             labels = c("Bloqu\u00E9"))
```


```{r}
glue::glue(issue_dev)
```

## Instructions pour *[Rel]*

```{r, eval=TRUE}
issue_dev <- paste(
'Cocher les cases au fur et à mesure

- [ ] Lire le chapitre "a - Exemple de section de texte à réviser"
  + [ ] https://gitlab.com/thinkr-open/demo.test.thinkr',
glue::glue('- [ ] Attendre que les issues #{n_proj_issues + 1} #{n_proj_issues + 2} #{n_proj_issues + 3} soient fermées'),
'- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "En cours"
- [ ] Récupérer le projet sur RStudio
    + File > New Project > Version Control > Git
- [ ] Faire un "pull" du projet et s\'assurer d\'être dans la branche _validation_
    + Onglet git
- [ ] Compiler le book en local pour voir s\'il fonctionne en l\'état
    + Onglet Build > Build Book > "pagedown::html_paged"
- [ ] Mettre une valeur positive pour "Construction / 2021_T1" dans le tableau nommé `les_data_prevision` dans la section "Des données en entrée"
- [ ] Ajouter une phrase dans le paragraphe avec les consignes qui te sont adressées.
    + [ ] Modifier la phrase: "Regardez ce graphique dans la figure", pour écrire le "titre du graphique (Fig. ref)".
- [ ] Ouvrir le mode de révision de git pour voir ses modifications
    + Onglet "Git" > "Diff"
- [ ] Ouvrir un ticket sur GitLab pour demander à changer le mot "négatif" par "positif"
      - [ ] Taguer les *[Prems]* pour une discussion à cet endroit, décider qui intègre les modifications, et demander de te prévenir quand c\'est décidé. La modification ne sera réalisée qu\'après l\'acceptation de la MR actuelle.',
paste0('```'),
'Titre: Le paragraphe `Du code qui crée des graphiques` nécessite des ajustements

Le chiffre de la construction est positif dans la nouvelle version.  
Peut-on modifier le paragraphe qui présente ce chiffre en conséquence ?  
Vous en pensez quoi ? @prems <= *A changer*

Si on est tous ok et quand ma MR sera acceptée, @prems-1 tu pourras t\'en charger, remettre à jour ma branche _revision_ et me prévenir sur Slack quand c\'est fait ? Merci.
',
paste0('```'),
'- [ ] Compiler le book en local
- [ ] Vérifier que la compilation et que les graphiques et tableaux sont bien modifiés.
- [ ] Créer un commit qui indique ce qu\'il fait et de fermer cette issue',
paste0('```'),
'Modification du tableau `les_data_prevision`

- La valeur du paramètre n\'était pas correcte',
paste0('- closes #', n_proj_issues + 4),
paste0('```'),
'- [ ] Envoyer les modifications sur GitLab
- [ ] S\'assurer que l\'intégration continue est au vert
- [ ] Vérifier que les modifications ont été apportées correctement au book _validation_
  - [ ] url du book **validation** à renseigner
- [ ] Assigner la révision de la MR à *[Resp]*
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "Révision"
- [ ] Mettre un message à *[Resp]* dans Slack pour lui indiquer qu\'il a une MR à réviser
- [ ] Quand les modifications sont ajoutées dans la branches _master_, on fera une nouvelle révision de texte avec le Web IDE de GitLab + Pages
    - [ ] Nous allons avoir 4 onglets ouverts: cette issue, le book _validation_ mis en forme, une nouvelle issue vide, le Web IDE pour apporter nos modifications  
    - [ ] Ouvrir la Merge Request qui t\'es assignée
    - [ ] Ouvrir le mode "Web IDE"
    - [ ] Regarder la différence entre le mode "Edit" ou "Review"
    - [ ] Modifier le paragraphe qui parle de `ref(tab:tab1)`
    - [ ] Commit
    - [ ] Vérifier que sur le book _validation_ c\'est pris en compte
    - [ ] Informer sur Slack que la MR de _validation_ est prête
- [ ] Fermer ce ticket quand la MR est acceptée
', sep = "\n")
```


```{r, eval=FALSE}
issue_dev_id <- gl_new_issue(title = "Instructions pour *[Rel]*",
                             project = project_id,
                             description = paste(issue_dev, collapse = "\n"),
                             labels = c("Bloqu\u00E9"))
```

```{r}
glue::glue(issue_dev)
```

## Instructions pour *[RedacChf]*

```{r, eval=TRUE}
issue_dev <- paste(
'Cocher les cases au fur et à mesure  
Nous allons avoir 4 onglets ouverts: cette issue, le book _production_ mis en forme, une nouvelle issue vide, le Web IDE pour apporter nos modifications  

- [ ] Lire le chapitre "a - Exemple de section de texte à réviser"
  + [ ] https://gitlab.com/thinkr-open/demo.test.thinkr',
glue::glue('- [ ] Attendre que l\'issue #{n_proj_issues + 4} soit fermée'),
'- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "En cours"
- [ ] Ouvrir un onglet avec la version actuelle du book _production_: url de la branche **production**
    + [ ] Toute modification que tu _commit_ sera visible ici
- [ ] Ouvrir un onglet pour ouvrir une nouvelle issue GitLab
    + [ ] On y mettra des commentaires au cours de notre révision
- [ ] Ouvrir un onglet avec la Merge Request qui t\'est assignée
    + [ ] Ouvrir le mode "Web IDE"
- [ ] Regarder la différence entre le mode "Edit" ou "Review"
- [ ] Modifier le texte qui contient `@ref(fig:fig1)` pour en faire une vraie phrase.
- [ ] Changer la légende de la figure 1 où il y a `(ref:fig1cap)`
- [ ] Ajouter un commentaire dans l\'issue qu\'on a ouverte pour demander à avoir le graphique en plus grand dans le rapport.
- [ ] Enregistrer nos modifications en créant un commit qui indique ce qu\'il fait et de fermer cette issue',
paste0('```'),
'Quelques ré-écriture de paragraphe

- Une phrase mieux tournée
- Légende de la figure mise à jour',
paste0('- closes #', n_proj_issues + 5),
paste0('```'),
'- [ ] S\'assurer que l\'intégration continue est au vert
- [ ] Vérifier que les modifications ont été apportées correctement au book _production_
  - [ ] url du book **production**
- [ ] Sur le "board" du projet GitLab, déplacer ce ticket dans la colonne "Révision"
- [ ] Envoyer un message sur Slack pour dire que la MR de _production_ est prête
- [ ] Fermer ce ticket quand la MR est acceptée
', sep = "\n")
```


```{r, eval=FALSE}
issue_dev_id <- gl_new_issue(title = "Instructions pour *[RedacChf]*",
                             project = project_id,
                             description = paste(issue_dev, collapse = "\n"),
                             labels = c("Bloqu\u00E9"))
```


```{r}
glue::glue(issue_dev)
```

## Wiki

- Créer un wiki avec ces informations:
```md
# Bienvenue sur le projet de création de rapport automatisé

Les différentes versions du rapport sont disponibles à cette adresse: https://thinkr-open.gitlab.io/demo.test.thinkr
```

